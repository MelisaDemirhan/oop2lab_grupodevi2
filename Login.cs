﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            textBoxPasswordLogin.PasswordChar = '*';
        }
        public static int control = 0;
        Database a = new Database();    
        private void buttonSignin_Click_1(object sender, EventArgs e)
        {           
            string name, password;
            name = textBoxNameLogin.Text;       
            password = textBoxPasswordLogin.Text;
            ActiveUser.getInstance().Active = a.getActiveUser(name, password);
            if (ActiveUser.getInstance().Active != null)
            {
                MessageBox.Show("Giriş başarılı alışverişe devam edebilirsiniz!");
                StartForm anaform = (StartForm)Application.OpenForms["StartForm"];
                anaform.buttonProfile.Visible = true;
                anaform.buttonShopping.Visible = true;
                anaform.buttonCart.Visible = true;
                if (ActiveUser.getInstance().Active.IsAdmin == true)
                {
                    anaform.buttonAdmin.Visible = true;
                }
                else
                {
                    anaform.buttonAdmin.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("Şifre ya da isim hatalı.Lütfen doğru kullanıcı bilgileri giriniz!");
            }
                     
            textBoxNameLogin.Text = "";
            textBoxPasswordLogin.Text = "";
        }

        private void checkBoxShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowPassword.Checked)
            {
                textBoxPasswordLogin.PasswordChar = '\0';
            }
            else
            {
                textBoxPasswordLogin.PasswordChar = '*';              
            }
        }
    }
}
