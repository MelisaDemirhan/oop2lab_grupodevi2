﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class Sign_up : Form
    {
        public Sign_up()
        {
            InitializeComponent();
            textBoxPasswordSign.PasswordChar = '*';
            textBoxConfirmPassword.PasswordChar = '*';
        }
        Database a = new Database();
        private void buttonSignup_Click(object sender, EventArgs e)
        {    
            string name, password;
            bool control = false;
            if (textBoxNameSign.Text == ""  || textBoxPasswordSign.Text == "" || textBoxConfirmPassword.Text == "")
            {
                MessageBox.Show("Lütfen bilgileri eksiksiz giriniz. ");
                return;
            }
            else 
            {
                if (textBoxPasswordSign.Text == textBoxConfirmPassword.Text)
                {
                    name = textBoxNameSign.Text;
                    password = textBoxPasswordSign.Text;
                    a.UserList();
                    List<User> user = a.getUserList();
                    foreach (User u in user)
                    {
                        if (u.UserName == name)
                            control = true;
                    }
                    if (control)
                    {
                        MessageBox.Show("Böyle bir kullanıcı zaten var!");
                        textBoxNameSign.Text = "";                   
                        textBoxPasswordSign.Text = "";
                        textBoxConfirmPassword.Text = "";
                        return;
                    }
                    else
                    {
                        User b  = new User { UserName = name, Password = password, Address = "", PhoneNumber = "", IsAdmin = false, ProfilePicture = "" };                       
                        textBoxNameSign.Text = "";
                        textBoxPasswordSign.Text = "";
                        textBoxConfirmPassword.Text = "";
                        a.AddUser(b);

                    }
                }
                else
                {
                    MessageBox.Show("Şifreler eşleşmiyor!");
                    textBoxNameSign.Text = "";
                    textBoxPasswordSign.Text = "";
                    textBoxConfirmPassword.Text = "";
                    return;
                }
            }     
        }

        private void checkBoxShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowPassword.Checked)
            {
                textBoxPasswordSign.PasswordChar = '\0';
                textBoxConfirmPassword.PasswordChar = '\0';
            }
            else
            {
                textBoxPasswordSign.PasswordChar = '*';
                textBoxConfirmPassword.PasswordChar = '*';
            }
        }
    }
}
