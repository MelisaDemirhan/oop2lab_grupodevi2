﻿
namespace OOPLAB2
{
    partial class ShoppingCrt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShoppingCrt));
            this.picCoverPage = new System.Windows.Forms.PictureBox();
            this.lblName = new System.Windows.Forms.Label();
            this.buttonDeleteProduct = new System.Windows.Forms.Button();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.lblHeaderUnitPrice = new System.Windows.Forms.Label();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.labelProductQuantity = new System.Windows.Forms.Label();
            this.lblHeaderTotalPrice = new System.Windows.Forms.Label();
            this.lblHeaderQuantity = new System.Windows.Forms.Label();
            this.labelProductNumber = new System.Windows.Forms.Label();
            this.buttonSubstractProduct = new System.Windows.Forms.Button();
            this.buttonAddProduct = new System.Windows.Forms.Button();
            this.lblISBN = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picCoverPage)).BeginInit();
            this.SuspendLayout();
            // 
            // picCoverPage
            // 
            this.picCoverPage.Location = new System.Drawing.Point(35, 16);
            this.picCoverPage.Margin = new System.Windows.Forms.Padding(4);
            this.picCoverPage.Name = "picCoverPage";
            this.picCoverPage.Size = new System.Drawing.Size(141, 122);
            this.picCoverPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCoverPage.TabIndex = 77;
            this.picCoverPage.TabStop = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblName.Location = new System.Drawing.Point(48, 142);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 33);
            this.lblName.TabIndex = 78;
            // 
            // buttonDeleteProduct
            // 
            this.buttonDeleteProduct.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonDeleteProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDeleteProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeleteProduct.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteProduct.Location = new System.Drawing.Point(17, 405);
            this.buttonDeleteProduct.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDeleteProduct.Name = "buttonDeleteProduct";
            this.buttonDeleteProduct.Size = new System.Drawing.Size(165, 33);
            this.buttonDeleteProduct.TabIndex = 76;
            this.buttonDeleteProduct.Text = "Delete Item";
            this.buttonDeleteProduct.UseVisualStyleBackColor = false;
            this.buttonDeleteProduct.Click += new System.EventHandler(this.buttonDeleteProduct_Click);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblUnitPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUnitPrice.Location = new System.Drawing.Point(75, 302);
            this.lblUnitPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(43, 33);
            this.lblUnitPrice.TabIndex = 75;
            this.lblUnitPrice.Text = "₺0";
            // 
            // lblHeaderUnitPrice
            // 
            this.lblHeaderUnitPrice.AutoSize = true;
            this.lblHeaderUnitPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderUnitPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeaderUnitPrice.Location = new System.Drawing.Point(48, 269);
            this.lblHeaderUnitPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderUnitPrice.Name = "lblHeaderUnitPrice";
            this.lblHeaderUnitPrice.Size = new System.Drawing.Size(113, 33);
            this.lblHeaderUnitPrice.TabIndex = 74;
            this.lblHeaderUnitPrice.Text = "Unit Price";
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.labelTotalPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelTotalPrice.Location = new System.Drawing.Point(75, 368);
            this.labelTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(43, 33);
            this.labelTotalPrice.TabIndex = 73;
            this.labelTotalPrice.Text = "₺0";
            // 
            // labelProductQuantity
            // 
            this.labelProductQuantity.AutoSize = true;
            this.labelProductQuantity.BackColor = System.Drawing.Color.Transparent;
            this.labelProductQuantity.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelProductQuantity.ForeColor = System.Drawing.Color.Black;
            this.labelProductQuantity.Location = new System.Drawing.Point(76, 217);
            this.labelProductQuantity.Name = "labelProductQuantity";
            this.labelProductQuantity.Size = new System.Drawing.Size(29, 33);
            this.labelProductQuantity.TabIndex = 72;
            this.labelProductQuantity.Text = "0";
            // 
            // lblHeaderTotalPrice
            // 
            this.lblHeaderTotalPrice.AutoSize = true;
            this.lblHeaderTotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderTotalPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeaderTotalPrice.Location = new System.Drawing.Point(35, 335);
            this.lblHeaderTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderTotalPrice.Name = "lblHeaderTotalPrice";
            this.lblHeaderTotalPrice.Size = new System.Drawing.Size(120, 33);
            this.lblHeaderTotalPrice.TabIndex = 71;
            this.lblHeaderTotalPrice.Text = "Total Price";
            // 
            // lblHeaderQuantity
            // 
            this.lblHeaderQuantity.AutoSize = true;
            this.lblHeaderQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderQuantity.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeaderQuantity.Location = new System.Drawing.Point(48, 184);
            this.lblHeaderQuantity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderQuantity.Name = "lblHeaderQuantity";
            this.lblHeaderQuantity.Size = new System.Drawing.Size(100, 33);
            this.lblHeaderQuantity.TabIndex = 70;
            this.lblHeaderQuantity.Text = "Quantity";
            // 
            // labelProductNumber
            // 
            this.labelProductNumber.AutoSize = true;
            this.labelProductNumber.BackColor = System.Drawing.Color.Transparent;
            this.labelProductNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelProductNumber.ForeColor = System.Drawing.Color.Black;
            this.labelProductNumber.Location = new System.Drawing.Point(78, 471);
            this.labelProductNumber.Name = "labelProductNumber";
            this.labelProductNumber.Size = new System.Drawing.Size(17, 18);
            this.labelProductNumber.TabIndex = 82;
            this.labelProductNumber.Text = "0";
            // 
            // buttonSubstractProduct
            // 
            this.buttonSubstractProduct.BackColor = System.Drawing.Color.Transparent;
            this.buttonSubstractProduct.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSubstractProduct.BackgroundImage")));
            this.buttonSubstractProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSubstractProduct.FlatAppearance.BorderSize = 0;
            this.buttonSubstractProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSubstractProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonSubstractProduct.Location = new System.Drawing.Point(42, 471);
            this.buttonSubstractProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSubstractProduct.Name = "buttonSubstractProduct";
            this.buttonSubstractProduct.Size = new System.Drawing.Size(31, 21);
            this.buttonSubstractProduct.TabIndex = 81;
            this.buttonSubstractProduct.Text = "-";
            this.buttonSubstractProduct.UseVisualStyleBackColor = false;
            this.buttonSubstractProduct.Click += new System.EventHandler(this.buttonSubstractProduct_Click);
            // 
            // buttonAddProduct
            // 
            this.buttonAddProduct.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddProduct.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonAddProduct.BackgroundImage")));
            this.buttonAddProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonAddProduct.FlatAppearance.BorderSize = 0;
            this.buttonAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonAddProduct.Location = new System.Drawing.Point(102, 471);
            this.buttonAddProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAddProduct.Name = "buttonAddProduct";
            this.buttonAddProduct.Size = new System.Drawing.Size(31, 21);
            this.buttonAddProduct.TabIndex = 80;
            this.buttonAddProduct.UseVisualStyleBackColor = false;
            this.buttonAddProduct.Click += new System.EventHandler(this.buttonAddProduct_Click);
            // 
            // lblISBN
            // 
            this.lblISBN.AutoSize = true;
            this.lblISBN.BackColor = System.Drawing.Color.Transparent;
            this.lblISBN.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblISBN.ForeColor = System.Drawing.Color.White;
            this.lblISBN.Location = new System.Drawing.Point(73, 471);
            this.lblISBN.Name = "lblISBN";
            this.lblISBN.Size = new System.Drawing.Size(0, 21);
            this.lblISBN.TabIndex = 79;
            // 
            // ShoppingCrt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.labelProductNumber);
            this.Controls.Add(this.buttonSubstractProduct);
            this.Controls.Add(this.buttonAddProduct);
            this.Controls.Add(this.lblISBN);
            this.Controls.Add(this.picCoverPage);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.buttonDeleteProduct);
            this.Controls.Add(this.lblUnitPrice);
            this.Controls.Add(this.lblHeaderUnitPrice);
            this.Controls.Add(this.labelTotalPrice);
            this.Controls.Add(this.labelProductQuantity);
            this.Controls.Add(this.lblHeaderTotalPrice);
            this.Controls.Add(this.lblHeaderQuantity);
            this.Name = "ShoppingCrt";
            this.Size = new System.Drawing.Size(222, 530);
            ((System.ComponentModel.ISupportInitialize)(this.picCoverPage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picCoverPage;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button buttonDeleteProduct;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.Label lblHeaderUnitPrice;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.Label labelProductQuantity;
        private System.Windows.Forms.Label lblHeaderTotalPrice;
        private System.Windows.Forms.Label lblHeaderQuantity;
        private System.Windows.Forms.Label labelProductNumber;
        private System.Windows.Forms.Button buttonSubstractProduct;
        private System.Windows.Forms.Button buttonAddProduct;
        private System.Windows.Forms.Label lblISBN;
    }
}
