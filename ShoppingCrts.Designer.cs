﻿
namespace OOPLAB2
{
    partial class ShoppingCrts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanelCrts = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPaymentAmount = new System.Windows.Forms.Label();
            this.buttonConfrimCart = new System.Windows.Forms.Button();
            this.buttonDeleteCart = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanelCrts
            // 
            this.flowLayoutPanelCrts.AutoScroll = true;
            this.flowLayoutPanelCrts.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanelCrts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flowLayoutPanelCrts.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelCrts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanelCrts.Name = "flowLayoutPanelCrts";
            this.flowLayoutPanelCrts.Size = new System.Drawing.Size(1157, 654);
            this.flowLayoutPanelCrts.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.labelPaymentAmount);
            this.panel2.Controls.Add(this.buttonConfrimCart);
            this.panel2.Controls.Add(this.buttonDeleteCart);
            this.panel2.Location = new System.Drawing.Point(0, 658);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1157, 110);
            this.panel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(32, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 33);
            this.label1.TabIndex = 4;
            this.label1.Text = "Payment Amount :";
            // 
            // labelPaymentAmount
            // 
            this.labelPaymentAmount.AutoSize = true;
            this.labelPaymentAmount.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelPaymentAmount.ForeColor = System.Drawing.Color.Black;
            this.labelPaymentAmount.Location = new System.Drawing.Point(241, 16);
            this.labelPaymentAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPaymentAmount.Name = "labelPaymentAmount";
            this.labelPaymentAmount.Size = new System.Drawing.Size(0, 33);
            this.labelPaymentAmount.TabIndex = 3;
            // 
            // buttonConfrimCart
            // 
            this.buttonConfrimCart.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonConfrimCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonConfrimCart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonConfrimCart.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonConfrimCart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonConfrimCart.Location = new System.Drawing.Point(285, 53);
            this.buttonConfrimCart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonConfrimCart.Name = "buttonConfrimCart";
            this.buttonConfrimCart.Size = new System.Drawing.Size(194, 42);
            this.buttonConfrimCart.TabIndex = 3;
            this.buttonConfrimCart.Text = "Confirm Cart";
            this.buttonConfrimCart.UseVisualStyleBackColor = false;
            this.buttonConfrimCart.Click += new System.EventHandler(this.buttonConfrimCart_Click);
            // 
            // buttonDeleteCart
            // 
            this.buttonDeleteCart.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonDeleteCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDeleteCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeleteCart.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonDeleteCart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteCart.Location = new System.Drawing.Point(20, 53);
            this.buttonDeleteCart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDeleteCart.Name = "buttonDeleteCart";
            this.buttonDeleteCart.Size = new System.Drawing.Size(194, 42);
            this.buttonDeleteCart.TabIndex = 2;
            this.buttonDeleteCart.Text = "Delete Cart";
            this.buttonDeleteCart.UseVisualStyleBackColor = false;
            this.buttonDeleteCart.Click += new System.EventHandler(this.buttonDeleteCart_Click);
            // 
            // ShoppingCrts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.flowLayoutPanelCrts);
            this.Name = "ShoppingCrts";
            this.Size = new System.Drawing.Size(1160, 770);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelCrts;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label labelPaymentAmount;
        private System.Windows.Forms.Button buttonConfrimCart;
        private System.Windows.Forms.Button buttonDeleteCart;
    }
}
