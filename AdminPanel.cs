﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class AdminPanel : Form
    {
        public AdminPanel()
        {
            InitializeComponent();
        }
        string photoLocation = null; //take location of products photo
        string photoFileName = "ProducImages"; //filename of products
        Database database = new Database();       

        private void AdminPanel_Load(object sender, EventArgs e)
        {          
            database.ProductList();
            List<Products> pp = new List<Products>();
            pp = database.getProductList();
            foreach (var item in pp)
            {
                listboxProducts.Items.Add(item.Name); //Add products which in database
            }
        }

        private bool controlIsTextEmpty(string name, string price, string description, string stok)
        {
            //a function to control  whether product's infos entering correctly or not.
            if (name == "" || price == "" || description == "" || stok == "")
            {
                MessageBox.Show("Please enter product's informations precisely.");
                return true;
            }
            return false;
        }

        private void listboxProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                database.ProductList();
                List<Products> pp = new List<Products>();
                pp = database.getProductList();
               
                //Show selected item's info to text box
                txtboxShow.Text = pp[listboxProducts.SelectedIndex].Name + Environment.NewLine
                                + pp[listboxProducts.SelectedIndex].Price + Environment.NewLine
                                + pp[listboxProducts.SelectedIndex].Description + Environment.NewLine
                                + pp[listboxProducts.SelectedIndex].Stock;

                ////find photo in given file with using Path.Combine and assign photo's location to photoLocation
                photoLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, pp[listboxProducts.SelectedIndex].ProductImage);
                pictureProduct.ImageLocation = photoLocation; //assign photoLocation to picturebox's ImageLocation properties
                photoLocation = null;
            }
            catch (Exception) { }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //controlling  whether product's infos entering correctly or not.
            if (controlIsTextEmpty(txtboxName.Text, txtboxPrice.Text, txtBoxDescription.Text, txtBoxStok.Text)) { return; }

            if (photoLocation == null)
            {
                MessageBox.Show("Please choose photo for product.");
            }
            else
            {
                foreach (var item in listboxProducts.Items)
                {
                    // if user trying to add existing product, alerted by the system.
                    if (item.ToString().ToLower() == txtboxName.Text.ToLower())
                    {
                        MessageBox.Show("This product already in the list if you want to update this product please choose Update option.");
                        return;
                    }
                }

                //Add new product to products list
                Products p = new Products
                {
                    Name = txtboxName.Text,
                    Price = Int32.Parse(txtboxPrice.Text),
                    Description = txtBoxDescription.Text,
                    Stock = Convert.ToInt32(txtBoxStok.Text),
                    ProductImage = photoLocation
                };              

                //Add new product which in products list to listbox                
                database.AddProduct(p);
                listboxProducts.Items.Clear();                  
                database.ProductList();
                List<Products> pp = new List<Products>();
                pp = database.getProductList();
                foreach (var item in pp)
                {
                    listboxProducts.Items.Add(item.Name);
                }           
                                 
                //Clear text boxes
                txtboxShow.Text = "";
                txtboxName.Text = "";
                txtboxPrice.Text = "";
                txtBoxDescription.Text = "";
                txtBoxStok.Text = "";
                //change picturebox's image with genel image
                photoLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, "genel.jpg");
                pictureProduct.ImageLocation = photoLocation;
                photoLocation = null;
               
            }           
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<Products> pp = new List<Products>();
            database.ProductList();
            pp = database.getProductList();
            //if user doesn't  choose a product ,alerted by the system.
            if (listboxProducts.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose a product to delete.");
            }
            else
            {
                int l = listboxProducts.SelectedIndex;
                             
                database.DeleteProduct(pp[l]);              
                database.AdminDeleteAllCartProduct(pp[l]);

                pp = new List<Products>();              
                database.ProductList();
                pp = database.getProductList();

                listboxProducts.Items.Clear();
                foreach (var item in pp)
                {
                    listboxProducts.Items.Add(item.Name);
                }
                //Clear text boxes
                txtboxShow.Text = "";
                txtboxName.Text = "";
                txtboxPrice.Text = "";
                txtBoxDescription.Text = "";
                txtBoxStok.Text = "";
                //change picturebox's image with genel image
                photoLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, "genel.jpg");
                pictureProduct.ImageLocation = photoLocation;
                photoLocation = null;
            }
        
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            List<Products> pp = new List<Products>();
            database.ProductList();
            pp = database.getProductList();
            //if user doesn't  choose a product ,alerted by the system.
            if (listboxProducts.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose a product to update.");
            }
            else
            {
                //if user hasn't changed any properties warned.
                if (txtboxName.Text == "" && txtboxPrice.Text == "" && txtBoxDescription.Text == "" && txtBoxStok.Text == "" && photoLocation == null)
                {
                    MessageBox.Show("You haven't changed any properties.");
                    return;
                }
                // to update  just the changed properties if condition used.
                if (txtboxName.Text != "")
                {
                    foreach (var item in listboxProducts.Items)
                    {
                        //if user trying to add existing product, alerted by the system.
                        if (item.ToString().ToLower() == txtboxName.Text.ToLower() && txtboxName.Text.ToLower() != pp[listboxProducts.SelectedIndex].Name.ToLower())
                        {
                            MessageBox.Show("There is already a product has this name on the list.");
                            return;
                        }
                    }
                    pp[listboxProducts.SelectedIndex].Name = txtboxName.Text;
                }
                if (txtboxPrice.Text != "")
                {
                   pp[listboxProducts.SelectedIndex].Price = Int32.Parse(txtboxPrice.Text);
                }
                if (txtBoxDescription.Text != "")
                {
                    pp[listboxProducts.SelectedIndex].Description = txtBoxDescription.Text;
                }
                if (txtBoxStok.Text != "")
                {
                    pp[listboxProducts.SelectedIndex].Stock = Convert.ToInt32(txtBoxStok.Text);
                }
                if (photoLocation != null)
                {
                    pp[listboxProducts.SelectedIndex].ProductImage = photoLocation;
                    pictureProduct.Image = Image.FromFile(photoLocation);
                }

                //change infos in listbox too
                database.UpdateProduct(pp[listboxProducts.SelectedIndex]);
                pp = new List<Products>();
                database.ProductList();
                pp = database.getProductList();

                listboxProducts.Items.Clear();
                foreach (var item in pp)
                {
                    listboxProducts.Items.Add(item.Name); //Add first 10 products to listbox
                }


            }

            //Clear text boxes and reset the settings.
            txtboxShow.Text = "";
            txtboxName.Text = "";
            txtboxPrice.Text = "";
            txtBoxDescription.Text = "";
            txtBoxStok.Text = "";
            //change picturebox's image with genel image
            photoLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, "genel.jpg");
            pictureProduct.ImageLocation = photoLocation;
            photoLocation = null;
           
        }

        private void buttonUploadI_Click(object sender, EventArgs e)
        {
            //user can choose just these type of files
            openFileDialog1.Filter = "Image Files(*.BMP;*.JPG;*.PNG;*.JPEG;*.ICO)|*.BMP;*.JPG;*.PNG;*.JPEG;*.ICO|All files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK) //control file type is suitable
            {
                photoLocation = openFileDialog1.FileName; //assign selected image's location to photoLocation
                pictureProduct.Image = Image.FromFile(photoLocation); //open selected image
            }
        }

        private void txtboxPrice_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            //price must be integer value.
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtBoxStok_KeyPress(object sender, KeyPressEventArgs e)
        {
            // stok must be integer value.
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
