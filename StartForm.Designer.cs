﻿
namespace OOPLAB2
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.buttonCart = new System.Windows.Forms.Button();
            this.buttonProfile = new System.Windows.Forms.Button();
            this.buttonShopping = new System.Windows.Forms.Button();
            this.buttonAdmin = new System.Windows.Forms.Button();
            this.buttonOut = new System.Windows.Forms.Button();
            this.buttonSignup = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonhome = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttondown = new System.Windows.Forms.Button();
            this.exitstart = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panelMenu.Controls.Add(this.buttonCart);
            this.panelMenu.Controls.Add(this.buttonProfile);
            this.panelMenu.Controls.Add(this.buttonShopping);
            this.panelMenu.Controls.Add(this.buttonAdmin);
            this.panelMenu.Controls.Add(this.buttonOut);
            this.panelMenu.Controls.Add(this.buttonSignup);
            this.panelMenu.Controls.Add(this.buttonLogin);
            this.panelMenu.Controls.Add(this.buttonhome);
            this.panelMenu.Controls.Add(this.panel4);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(4);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(219, 807);
            this.panelMenu.TabIndex = 3;
            // 
            // buttonCart
            // 
            this.buttonCart.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonCart.FlatAppearance.BorderSize = 0;
            this.buttonCart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCart.Location = new System.Drawing.Point(0, 477);
            this.buttonCart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCart.Name = "buttonCart";
            this.buttonCart.Size = new System.Drawing.Size(215, 58);
            this.buttonCart.TabIndex = 14;
            this.buttonCart.Text = "My Cart";
            this.buttonCart.UseVisualStyleBackColor = false;
            this.buttonCart.Visible = false;
            this.buttonCart.Click += new System.EventHandler(this.buttonCart_Click);
            // 
            // buttonProfile
            // 
            this.buttonProfile.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonProfile.FlatAppearance.BorderSize = 0;
            this.buttonProfile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonProfile.Location = new System.Drawing.Point(0, 533);
            this.buttonProfile.Margin = new System.Windows.Forms.Padding(4);
            this.buttonProfile.Name = "buttonProfile";
            this.buttonProfile.Size = new System.Drawing.Size(215, 58);
            this.buttonProfile.TabIndex = 13;
            this.buttonProfile.Text = "Profile";
            this.buttonProfile.UseVisualStyleBackColor = false;
            this.buttonProfile.Visible = false;
            this.buttonProfile.Click += new System.EventHandler(this.buttonProfile_Click);
            // 
            // buttonShopping
            // 
            this.buttonShopping.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonShopping.FlatAppearance.BorderSize = 0;
            this.buttonShopping.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonShopping.Location = new System.Drawing.Point(0, 423);
            this.buttonShopping.Margin = new System.Windows.Forms.Padding(4);
            this.buttonShopping.Name = "buttonShopping";
            this.buttonShopping.Size = new System.Drawing.Size(215, 58);
            this.buttonShopping.TabIndex = 12;
            this.buttonShopping.Text = "Shopping";
            this.buttonShopping.UseVisualStyleBackColor = false;
            this.buttonShopping.Visible = false;
            this.buttonShopping.Click += new System.EventHandler(this.buttonShopping_Click);
            // 
            // buttonAdmin
            // 
            this.buttonAdmin.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonAdmin.FlatAppearance.BorderSize = 0;
            this.buttonAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAdmin.Location = new System.Drawing.Point(0, 590);
            this.buttonAdmin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAdmin.Name = "buttonAdmin";
            this.buttonAdmin.Size = new System.Drawing.Size(215, 58);
            this.buttonAdmin.TabIndex = 11;
            this.buttonAdmin.Text = "Admin";
            this.buttonAdmin.UseVisualStyleBackColor = false;
            this.buttonAdmin.Visible = false;
            this.buttonAdmin.Click += new System.EventHandler(this.buttonAdmin_Click);
            // 
            // buttonOut
            // 
            this.buttonOut.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonOut.FlatAppearance.BorderSize = 0;
            this.buttonOut.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOut.Location = new System.Drawing.Point(0, 366);
            this.buttonOut.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOut.Name = "buttonOut";
            this.buttonOut.Size = new System.Drawing.Size(215, 58);
            this.buttonOut.TabIndex = 10;
            this.buttonOut.Text = "Sign out";
            this.buttonOut.UseVisualStyleBackColor = false;
            this.buttonOut.Click += new System.EventHandler(this.buttonOut_Click);
            // 
            // buttonSignup
            // 
            this.buttonSignup.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonSignup.FlatAppearance.BorderSize = 0;
            this.buttonSignup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSignup.Location = new System.Drawing.Point(0, 309);
            this.buttonSignup.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSignup.Name = "buttonSignup";
            this.buttonSignup.Size = new System.Drawing.Size(215, 58);
            this.buttonSignup.TabIndex = 9;
            this.buttonSignup.Text = "Sign up";
            this.buttonSignup.UseVisualStyleBackColor = false;
            this.buttonSignup.Click += new System.EventHandler(this.buttonSignup_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLogin.Location = new System.Drawing.Point(0, 252);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(215, 58);
            this.buttonLogin.TabIndex = 8;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonhome
            // 
            this.buttonhome.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonhome.FlatAppearance.BorderSize = 0;
            this.buttonhome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonhome.Location = new System.Drawing.Point(0, 195);
            this.buttonhome.Margin = new System.Windows.Forms.Padding(4);
            this.buttonhome.Name = "buttonhome";
            this.buttonhome.Size = new System.Drawing.Size(215, 58);
            this.buttonhome.TabIndex = 7;
            this.buttonhome.Text = "Home";
            this.buttonhome.UseVisualStyleBackColor = false;
            this.buttonhome.Click += new System.EventHandler(this.buttonhome_Click_1);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(219, 196);
            this.panel4.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(216, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1048, 36);
            this.panel2.TabIndex = 4;
            // 
            // buttondown
            // 
            this.buttondown.BackColor = System.Drawing.Color.SlateBlue;
            this.buttondown.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttondown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttondown.Location = new System.Drawing.Point(1261, 0);
            this.buttondown.Margin = new System.Windows.Forms.Padding(4);
            this.buttondown.Name = "buttondown";
            this.buttondown.Size = new System.Drawing.Size(60, 37);
            this.buttondown.TabIndex = 6;
            this.buttondown.Text = "-";
            this.buttondown.UseVisualStyleBackColor = false;
            this.buttondown.Click += new System.EventHandler(this.buttondown_Click);
            // 
            // exitstart
            // 
            this.exitstart.BackColor = System.Drawing.Color.SlateBlue;
            this.exitstart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.exitstart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.exitstart.Location = new System.Drawing.Point(1316, 0);
            this.exitstart.Margin = new System.Windows.Forms.Padding(4);
            this.exitstart.Name = "exitstart";
            this.exitstart.Size = new System.Drawing.Size(60, 37);
            this.exitstart.TabIndex = 5;
            this.exitstart.Text = "X";
            this.exitstart.UseVisualStyleBackColor = false;
            this.exitstart.Click += new System.EventHandler(this.exitstart_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(216, 37);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1160, 770);
            this.panel1.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1374, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(46, 807);
            this.panel3.TabIndex = 5;
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1420, 807);
            this.Controls.Add(this.buttondown);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.exitstart);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StartForm";
            this.panelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button exitstart;
        private System.Windows.Forms.Button buttonOut;
        private System.Windows.Forms.Button buttonSignup;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonhome;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttondown;
        public System.Windows.Forms.Button buttonProfile;
        public System.Windows.Forms.Button buttonShopping;
        public System.Windows.Forms.Button buttonAdmin;
        public System.Windows.Forms.Button buttonCart;
        private System.Windows.Forms.Panel panel3;
    }
}