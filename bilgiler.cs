﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class bilgiler : Form
    {
        string photoFileName = "ProducImages"; //filename of products
        public bilgiler(Products p)
        {
            InitializeComponent();
            labelName.Text = p.Name;
            labelPrice.Text = p.Price.ToString();
            labelStok.Text = p.Stock.ToString();
            labelDescription.Text = p.Description;
            pictureBilgiler.ImageLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, p.ProductImage);

        }

        private void exitstart_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
