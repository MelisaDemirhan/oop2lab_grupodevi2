﻿
namespace OOPLAB2
{
    partial class ProductsForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanelShopping = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // flowLayoutPanelShopping
            // 
            this.flowLayoutPanelShopping.AutoScroll = true;
            this.flowLayoutPanelShopping.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelShopping.Name = "flowLayoutPanelShopping";
            this.flowLayoutPanelShopping.Size = new System.Drawing.Size(1158, 713);
            this.flowLayoutPanelShopping.TabIndex = 0;
            // 
            // ProductsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanelShopping);
            this.Name = "ProductsForm";
            this.Size = new System.Drawing.Size(1158, 713);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanelShopping;
    }
}
