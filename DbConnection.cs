﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace OOPLAB2
{
    class SqlConnections
    {
        private SqlConnection connect;

        public SqlConnection Connect { get => connect; set => connect = value; }

        public void Connection()
        {
            var connectionString = @"Data Source=SQL5053.site4now.net; Initial Catalog=db_a75d2f_ooplab; User Id=db_a75d2f_ooplab_admin; Password=ooplab123";
            connect = new SqlConnection(connectionString);
        }

        public void Open()
        {
            connect.Open();
        }

        public void Close()
        {
            connect.Close();
        }
    }
}
