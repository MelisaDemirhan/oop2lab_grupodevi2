﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLAB2
{
    public class ItemToPurchase
    {
        private Products product;
        private int amount;
        public Products Product { get => product; set => product = value; }
        public int Amount { get => amount; set => amount = value; }
    }
}
