﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace OOPLAB2
{
    public class Cart
    {
        private static List<ItemToPurchase> itemsToPurchase = new List<ItemToPurchase>();
        private string userID;
        private double paymentAmount;
       
        
        public string UserID { get => userID; set => userID = value; }
        public double PaymentAmount { get => paymentAmount; set => paymentAmount = value; }
    
        public static List<ItemToPurchase> ItemsToPurchase { get => itemsToPurchase; set => itemsToPurchase = value; }

        public NumberFormatInfo p = new NumberFormatInfo();
       
        public Cart(string _CustomerId, double _paymentAmount, List<ItemToPurchase> _ItemsToPurchase)
        {
            userID = _CustomerId;
            paymentAmount = _paymentAmount;
            ItemsToPurchase = _ItemsToPurchase;
        }
        public Cart()
        {
            userID = ActiveUser.getInstance().Active.Id.ToString();
            itemsToPurchase = new List<ItemToPurchase>();
            paymentAmount = 0;
        }

        
        public void addProduct(ItemToPurchase addProduct)
        {
            try
            {
                p.NumberDecimalSeparator = ".";
                foreach (ItemToPurchase item in ItemsToPurchase)
                {
                    if (item.Product.Name == addProduct.Product.Name)
                    {
                        paymentAmount += addProduct.Amount * double.Parse(item.Product.Price.ToString(), p);
                        item.Amount += addProduct.Amount;
                        return;
                    }
                }
                ItemsToPurchase.Add(addProduct);
                paymentAmount += addProduct.Amount * double.Parse(addProduct.Product.Price.ToString(), p);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        public void removeProduct(ItemToPurchase removeProduct)
        {
            try
            {
                p.NumberDecimalSeparator = ".";

                foreach (ItemToPurchase item in ItemsToPurchase)
                {
                    if (item.Product.Name == removeProduct.Product.Name)
                    {
                        if (item.Amount > removeProduct.Amount)
                        {
                            paymentAmount -= removeProduct.Amount * double.Parse(item.Product.Price.ToString(), p);
                            item.Amount -= removeProduct.Amount;
                            return;
                        }
                        else
                        {
                            paymentAmount -= item.Amount * double.Parse(item.Product.Price.ToString(), p);
                            ItemsToPurchase.Remove(item);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        public void cancelOrder()
        {
            paymentAmount = 0;
            ItemsToPurchase.Clear();
        }
     
    }
}