﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class ProfileForm : Form
    {
        public ProfileForm()
        {
            InitializeComponent();         
        }
        string photoLocation = null; 
        Database database = new Database();
        private void buttonLoad_Click(object sender, EventArgs e)
        {   
            //user can choose just these type of files
            openFileDialog1.Filter = "Image Files(*.BMP;*.JPG;*.PNG;*.JPEG;*.ICO)|*.BMP;*.JPG;*.PNG;*.JPEG;*.ICO|All files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK) //control file type is suitable
            {
                photoLocation = openFileDialog1.FileName; 
                pictureBoxProfilePic.Image = Image.FromFile(photoLocation);
                ActiveUser.getInstance().Active.ProfilePicture = photoLocation;
            }
        }

        private void ProfileForm_Load(object sender, EventArgs e)
        {
            labelUserName.Text = ActiveUser.getInstance().Active.UserName;
            textBoxAddress.Text = ActiveUser.getInstance().Active.Address;
            textBoxPhoneNumber.Text = ActiveUser.getInstance().Active.PhoneNumber;
            try
            {
                pictureBoxProfilePic.Image = Image.FromFile(ActiveUser.getInstance().Active.ProfilePicture);
            }
            catch (Exception)
            {
            }           
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            
            ActiveUser.getInstance().Active.Address = textBoxAddress.Text;
            ActiveUser.getInstance().Active.PhoneNumber = textBoxPhoneNumber.Text;           
            database.UpdateUser(ActiveUser.getInstance().Active);
            MessageBox.Show("Changes are saved.");
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialog = new DialogResult();
            dialog = MessageBox.Show("Do you want to delete your account?", "Delete Account", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                ShoppingCrt.cart.cancelOrder();
                database.DeleteAllCartProductList();
                database.DeleteUser(ActiveUser.getInstance().Active);
                StartForm f = (StartForm)Application.OpenForms["StartForm"];
                ActiveUser.getInstance().Active = null;
                f.buttonAdmin.Visible = false;
                f.buttonShopping.Visible = false;
                f.buttonProfile.Visible = false;
                f.buttonCart.Visible = false;                                              
                f.OpenChildForm(new OOPLAB2.Signout(), sender);
            }            
        }

        private void labelChangePassword_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
        }

        private void buttonSavePassword_Click(object sender, EventArgs e)
        {
            if (textBoxNewPassword.Text == "" || textBoxConfirmPassword.Text == "")
            {
                MessageBox.Show("Fill all places");
                return;
            }
            if (textBoxConfirmPassword.Text != ActiveUser.getInstance().Active.Password)
            {
                MessageBox.Show("Password is not confirmed.");
                return;
            }
            ActiveUser.getInstance().Active.Password = textBoxNewPassword.Text;
            database.UpdateUser(ActiveUser.getInstance().Active);
            MessageBox.Show("Password is changed.");
        }

        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            StartForm f = (StartForm)Application.OpenForms["StartForm"];
            f.OpenChildForm(new OOPLAB2.Sign_up(), sender);
        }
    }
}
