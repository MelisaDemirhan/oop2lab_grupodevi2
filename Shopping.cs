﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class Shopping : Form
    {
        public Shopping()
        {
            InitializeComponent();

            panelshop.Controls.Clear();
            panelshop.AutoScroll = true;
            try
            {
                panelshop.Controls.Add(UploadAllProductsInfos(UploadProductsInfos()));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        public static List<Products> UploadProductsInfos()
        {
            Database database = new Database();
            database.ProductList();
            return database.getProductList();
        }
        
        public static ProductsForm UploadAllProductsInfos(List<Products> ProductList)
        {
            ProductsForm product = new ProductsForm();
            foreach (Products _product in ProductList)
            {
                product.flowLayoutPanelShopping.Controls.Add(new ProductForm(_product));
            }
            product.Dock = DockStyle.Fill;
            return product;
        }
    }
}


