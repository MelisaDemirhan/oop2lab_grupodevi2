﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace OOPLAB2
{
    public partial class ShoppingCrt : UserControl
    {
        public static Cart cart = new Cart(ActiveUser.getInstance().Active.Id.ToString(),0, Cart.ItemsToPurchase);

        Products _product;
        public NumberFormatInfo p = new NumberFormatInfo();
        Database database = new Database();
        List<Products> pro = new List<Products>();
        public ShoppingCrt(ItemToPurchase purchase)
        {
            InitializeComponent();
            try
            {
                string photoFileName = "ProducImages"; //filename of products
                p.NumberDecimalSeparator = ".";
                lblName.Text = purchase.Product.Name;
                labelProductQuantity.Text = purchase.Amount.ToString();
                labelProductNumber.Text = purchase.Amount.ToString();
                lblUnitPrice.Text = purchase.Product.Price + " ₺";
                labelTotalPrice.Text = ((purchase.Amount * double.Parse(purchase.Product.Price.ToString(), p)) + " ₺").ToString();
                picCoverPage.ImageLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, purchase.Product.ProductImage);               
                _product = purchase.Product;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
                   
        private void buttonAddProduct_Click(object sender, EventArgs e)
        {
            database.ProductList();
            pro = database.getProductList();
            foreach (var item in pro)
            {
                if (lblName.Text == item.Name)
                {
                    database.CartProductList();
                    DataTable a = database.getCartProduct();
                    database.ProductList();                 
                    ShoppingCrt.cart = new Cart();
                    for (int i = 0; i < a.Rows.Count; i++)
                    {
                        if (Int32.Parse(a.Rows[i][1].ToString()) == item.Id && Int32.Parse(a.Rows[i][2].ToString()) == item.Stock)
                        {
                            MessageBox.Show("No more product in stock!");
                            return;
                        }
                    }

                    labelProductNumber.Text = (int.Parse(labelProductNumber.Text) + 1).ToString();
                    labelProductQuantity.Text = (int.Parse(labelProductQuantity.Text) + 1).ToString();

                    ItemToPurchase itemTP = new ItemToPurchase();
                    itemTP.Product = item;
                    itemTP.Amount = 1;
                    ShoppingCrt.cart.addProduct(itemTP);
                    database.SaveCartProduct(itemTP);
                }
            }

            StartForm f = (StartForm)Application.OpenForms["StartForm"];
            f.buttonCart.PerformClick();
        }

        private void buttonSubstractProduct_Click(object sender, EventArgs e)
        {
            if (labelProductQuantity.Text == "1")
            {
                buttonDeleteProduct_Click(sender, e);
                return;
            }
            labelProductNumber.Text = (int.Parse(labelProductNumber.Text) - 1).ToString();
            labelProductQuantity.Text = (int.Parse(labelProductQuantity.Text) - 1).ToString();
            database.ProductList();
            pro = database.getProductList();
            foreach (var item in pro)
            {
                if (lblName.Text == item.Name)
                {
                    ItemToPurchase itemTP = new ItemToPurchase();
                    itemTP.Product = item;
                    itemTP.Amount = 1;
                    ShoppingCrt.cart.removeProduct(itemTP);
                    database.DeleteCartProductList(itemTP, 1);
                }
            }
            StartForm f = (StartForm)Application.OpenForms["StartForm"];
            f.buttonCart.PerformClick();
        }

        private void buttonDeleteProduct_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dResults = new DialogResult();
                dResults = MessageBox.Show("Product are deleted from your cart, are you sure?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dResults == DialogResult.Yes)
                {
                    for (int i = 0; i < int.Parse(labelProductQuantity.Text); i++)
                    {
                        ItemToPurchase item = new ItemToPurchase();
                        item.Product = _product;
                        item.Amount = 1;
                        cart.removeProduct(item);
                        ShoppingCrts.UploadShopListToCart();
                        database.DeleteCartProductList(item, 0);
                    }
                    StartForm f = (StartForm)Application.OpenForms["StartForm"];
                    f.buttonCart.PerformClick();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

