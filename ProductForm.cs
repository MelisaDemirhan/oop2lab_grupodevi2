﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{

    public partial class ProductForm : UserControl
    {
        string photoFileName = "ProducImages"; //filename of products
        public ProductForm(Products product)
        {
            InitializeComponent();
            img.ImageLocation = Path.Combine(Environment.CurrentDirectory, @photoFileName, product.ProductImage);
            labelName.Text = product.Name;
           if(product.Stock <= 0)
           {
                labelSoldOut.Visible = true;
                buttonAddToCart.Visible = false;
           }
        }
        Database database = new Database();
        List<Products> pro = new List<Products>();

        private void buttonAddToCart_Click(object sender, EventArgs e)
        {
            database.ProductList();
            pro = database.getProductList();
            foreach (var item in pro)
            {
                if (labelName.Text == item.Name)
                {
                    database.CartProductList();
                    DataTable a = database.getCartProduct();
                    database.ProductList();                  
                    ShoppingCrt.cart = new Cart();
                    for (int i = 0; i < a.Rows.Count; i++)
                    {                      
                        if (Int32.Parse(a.Rows[i][1].ToString()) == item.Id && Int32.Parse(a.Rows[i][2].ToString())==item.Stock)
                        {
                            MessageBox.Show("No more product in stock!");
                            return;
                        }                       
                    }
                    ItemToPurchase itemTP = new ItemToPurchase();
                    itemTP.Product = item;
                    itemTP.Amount = 1;
                    database.SaveCartProduct(itemTP);
                    MessageBox.Show("Product added to your cart.");

                }
            }
        }

        private void img_Click(object sender, EventArgs e)
        {
            database.ProductList();
            pro = database.getProductList();
            foreach (var item in pro)
            {
                if (labelName.Text == item.Name)
                {
                    bilgiler b = new bilgiler(item);
                    b.Show();
                }
            }         
        }
    }
}
