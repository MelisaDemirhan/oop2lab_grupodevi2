﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public class ActiveUser
    {
        private User active;
        private static ActiveUser activeUser;
        
        public User Active { get => active; set => active = value; }

        public static ActiveUser getInstance()
        {
            if (activeUser == null)
            {
                activeUser = new ActiveUser();
            }
            return activeUser;
        }
    }
}
