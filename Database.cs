﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace OOPLAB2
{
    public class Database
    {
        private SqlConnections connection = new SqlConnections();
        private DataTable userTable = new DataTable();
        private DataTable cartTable = new DataTable();
        private DataTable productTable = new DataTable();

        private User user;
        private Products product;

        private List<Products> products = new List<Products>();
        private List<User> users = new List<User>();


        public List<User> getUserList()
        {
            return users;
        }

        public User getActiveUser(string username, string password)
        {
            try
            {
                userTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter($"SELECT * FROM userTable WHERE " +
                    $"[username] = '{username}' AND [password] = '{password}';", connection.Connect);
                da.Fill(userTable);
                connection.Close();
                users = new List<User>();
                for (int i = 0; i < userTable.Rows.Count; i++)
                {
                    user = new User(userTable.Rows[i]["username"].ToString(), userTable.Rows[i]["password"].ToString(),
                        userTable.Rows[i]["address"].ToString(), Int32.Parse(userTable.Rows[i]["ID"].ToString()),
                         Convert.ToBoolean(userTable.Rows[i]["isAdmin"]), userTable.Rows[i]["pnumber"].ToString(), userTable.Rows[i]["photo"].ToString());
                }
                return user;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
      
        public void UserList()
        {
            try
            {

                userTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * From userTable", connection.Connect);
                da.Fill(userTable);
                connection.Close();
                users = new List<User>();
                for (int i = 0; i < userTable.Rows.Count; i++)
                {
                    user = new User(userTable.Rows[i]["username"].ToString(), userTable.Rows[i]["password"].ToString(),
                       userTable.Rows[i]["address"].ToString(), Int32.Parse(userTable.Rows[i]["ID"].ToString()),
                        Convert.ToBoolean(userTable.Rows[i]["isAdmin"]), userTable.Rows[i]["pnumber"].ToString(), userTable.Rows[i]["photo"].ToString());
                    users.Add(user);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddUser(User customer)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO userTable VALUES(@username, @password, @address,@pnumber, @isAdmin, @photo)", connection.Connect);
                command.Parameters.AddWithValue("@username", customer.UserName.ToString());
                command.Parameters.AddWithValue("@password", customer.Password.ToString());
                command.Parameters.AddWithValue("@address", customer.Address);
                command.Parameters.AddWithValue("@pnumber", customer.PhoneNumber);
                command.Parameters.AddWithValue("@isAdmin", customer.IsAdmin);
                command.Parameters.AddWithValue("@photo", customer.ProfilePicture);
                command.ExecuteNonQuery();
                connection.Close();
                //MessageBox.Show("Registration successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show("Başarılı bir şekilde kaydedildiniz,giriş yapabilirsiniz.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void UpdateUser(User user)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"UPDATE userTable SET username='{user.UserName}', " +
                $"password='{user.Password}', address='{user.Address}', pnumber='{user.PhoneNumber}', " +
                $"isAdmin='{user.IsAdmin}', photo='{user.ProfilePicture}' WHERE ID='{user.Id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                //MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        public void DeleteUser(User user)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"Delete from userTable WHERE [ID] = '{user.Id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                //MessageBox.Show("User deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        public List<Products> getProductList()
        {
            return products;
        }
      
        public void ProductList()
        {
            try
            {
                productTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("select * from dbo.product", connection.Connect);
                da.Fill(productTable);
                connection.Close();
                products = new List<Products>();
                for (int i = 0; i < productTable.Rows.Count; i++)
                {
                    product = new Products(Int32.Parse(productTable.Rows[i]["ID"].ToString()),
                        productTable.Rows[i]["name"].ToString(), Int32.Parse(productTable.Rows[i]["price"].ToString()),
                        productTable.Rows[i]["description"].ToString(),
                        productTable.Rows[i]["image"].ToString(), Int32.Parse(productTable.Rows[i]["stock"].ToString()));
                    products.Add(product);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddProduct(Products product)
        {
            try
            {

                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into dbo.product" +
                    " values(@name, @price, @description, @image, @stock)", connection.Connect);
                command.Parameters.AddWithValue("@name", product.Name);
                command.Parameters.AddWithValue("@price", product.Price);
                command.Parameters.AddWithValue("@description", product.Description);
                command.Parameters.AddWithValue("@image", product.ProductImage);
                command.Parameters.AddWithValue("@stock", product.Stock);
                command.ExecuteNonQuery();
                connection.Close();
               // MessageBox.Show("Registration successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
     
        public void UpdateProduct(Products product)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"UPDATE dbo.product SET name='{product.Name}', " +
                $"price='{product.Price}', description='{product.Description}', image='{product.ProductImage}', " +
                $"stock='{product.Stock}' WHERE ID='{product.Id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
               // MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        public void DeleteProduct(Products product)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"Delete from dbo.product WHERE [ID] = '{product.Id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                //MessageBox.Show("Product deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public DataTable getCartProduct()
        {
            return cartTable;
        }
        public void CartProductList()
        {
            try
            {
                cartTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * from cartTable where userID=" + ActiveUser.getInstance().Active.Id, connection.Connect);
                da.Fill(cartTable);
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void SaveCartProduct(ItemToPurchase itp)
        {
            try
            {
                CartProductList();
                int temp = 0;
                for (int i = 0; i < cartTable.Rows.Count; i++)
                {
                    if (cartTable.Rows[i][1].ToString() == itp.Product.Id.ToString())
                    {
                        temp = 1;
                        break;
                    }

                }
                if (temp == 1)
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command = new SqlCommand($"UPDATE cartTable SET amount = amount+1 where" +
                    $" [productID]= " + itp.Product.Id + " AND [userID] = " + ActiveUser.getInstance().Active.Id, connection.Connect);
                    command.ExecuteNonQuery();
                    connection.Close();
                   // MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command2 = new SqlCommand("Insert into cartTable values(@userID," +
                    "@productID,@amount)", connection.Connect);
                    command2.Parameters.AddWithValue("@userID", ActiveUser.getInstance().Active.Id);
                    command2.Parameters.AddWithValue("@productID", itp.Product.Id);
                    command2.Parameters.AddWithValue("@amount", itp.Amount);
                    command2.ExecuteNonQuery();
                    connection.Close();
                    //MessageBox.Show("Add successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        public void DeleteCartProductList(ItemToPurchase itp, int a)
        {
            try
            {
                connection.Connection();
                connection.Open();
                CartProductList();
                int temp = 0;
                for (int i = 0; i < cartTable.Rows.Count; i++)
                {
                    if (cartTable.Rows[i][1].ToString() == itp.Product.Id.ToString() &&
                        Int32.Parse(cartTable.Rows[i][2].ToString()) > 1)
                    {
                        temp = 1;
                    }
                }
                if (temp == 1)
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command = new SqlCommand($"UPDATE cartTable SET amount = amount-1 where" +
                    $" [productID]= " + itp.Product.Id + " AND [userID] = " + ActiveUser.getInstance().Active.Id, connection.Connect);
                    command.ExecuteNonQuery();
                    connection.Close();
                    //MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (temp == 0 || a == 0)
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command2 = new SqlCommand($"DELETE FROM cartTable where" +
                    $"[productID]= " + itp.Product.Id + " AND [userID] = " + ActiveUser.getInstance().Active.Id, connection.Connect);
                    command2.ExecuteNonQuery();
                    connection.Close();
                    //MessageBox.Show("Product deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DeleteAllCartProductList()
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM cartTable where userID=" + ActiveUser.getInstance().Active.Id, connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                //MessageBox.Show("Cart Deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void AdminDeleteAllCartProduct(Products p)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM cartTable where productID=" + p.Id, connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                //MessageBox.Show("Product Deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}