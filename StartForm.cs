﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class StartForm : Form
    {
        public StartForm()
        {

            InitializeComponent();
        }
        private Button currentButton;
        private Form activeForm;
        private void ActivateButton(object btnSender)
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton();
                    currentButton = (Button)btnSender;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                }
            }
        }
        private void DisableButton()
        {
            foreach (Control previousBtn in panelMenu.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = Color.FromArgb(153, 180, 209);
                    previousBtn.ForeColor = Color.Black;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                }
            }
        }
        public void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            ActivateButton(btnSender);
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(childForm);
            this.panel1.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
       
        private void buttonhome_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new OOPLAB2.Home(), sender);
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            OpenChildForm(new OOPLAB2.Login(), sender);
        }

        private void buttonSignup_Click(object sender, EventArgs e)
        {
            OpenChildForm(new OOPLAB2.Sign_up(), sender);
        }

        private void buttonAdmin_Click(object sender, EventArgs e)
        {
            OpenChildForm(new OOPLAB2.AdminPanel(), sender);
        }

        private void exitstart_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonShopping_Click(object sender, EventArgs e)
        {
            OpenChildForm(new OOPLAB2.Shopping(), sender);
        }

        private void buttonOut_Click(object sender, EventArgs e)
        {
            DialogResult dialog = new DialogResult();
            dialog = MessageBox.Show("Do you want to quit from account?", "Sign Out", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                ActiveUser.getInstance().Active = null;
                buttonAdmin.Visible = false;
                buttonShopping.Visible = false;
                buttonProfile.Visible = false;
                buttonCart.Visible = false;
                OpenChildForm(new OOPLAB2.Signout(), sender);
            }         
        }

        private void buttondown_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonProfile_Click(object sender, EventArgs e)
        {
            OpenChildForm(new OOPLAB2.ProfileForm(), sender);
        }

        public void buttonCart_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            if (panel1.Controls["ShoppingCrts"] == null)
            {
                panel1.Controls.Add(ShoppingCrts.UploadShopListToCart());
            }
            else
            {
                panel1.Controls.RemoveByKey("ShoppingCrts");
                panel1.Controls.Add(ShoppingCrts.UploadShopListToCart());
            }
            panel1.Controls["ShoppingCrts"].BringToFront();

        }
    }
}


    
    
