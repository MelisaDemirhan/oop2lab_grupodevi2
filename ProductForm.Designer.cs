﻿
namespace OOPLAB2
{
    partial class ProductForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSoldOut = new System.Windows.Forms.Label();
            this.buttonAddToCart = new System.Windows.Forms.Button();
            this.img = new System.Windows.Forms.PictureBox();
            this.labelName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
            this.SuspendLayout();
            // 
            // labelSoldOut
            // 
            this.labelSoldOut.AutoSize = true;
            this.labelSoldOut.BackColor = System.Drawing.Color.Transparent;
            this.labelSoldOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelSoldOut.ForeColor = System.Drawing.Color.Black;
            this.labelSoldOut.Location = new System.Drawing.Point(70, 231);
            this.labelSoldOut.Name = "labelSoldOut";
            this.labelSoldOut.Size = new System.Drawing.Size(78, 18);
            this.labelSoldOut.TabIndex = 42;
            this.labelSoldOut.Text = "Sold Out!";
            this.labelSoldOut.Visible = false;
            // 
            // buttonAddToCart
            // 
            this.buttonAddToCart.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.buttonAddToCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonAddToCart.FlatAppearance.BorderSize = 0;
            this.buttonAddToCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddToCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonAddToCart.Location = new System.Drawing.Point(47, 251);
            this.buttonAddToCart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAddToCart.Name = "buttonAddToCart";
            this.buttonAddToCart.Size = new System.Drawing.Size(127, 50);
            this.buttonAddToCart.TabIndex = 40;
            this.buttonAddToCart.Text = "Add to Cart";
            this.buttonAddToCart.UseVisualStyleBackColor = false;
            this.buttonAddToCart.Click += new System.EventHandler(this.buttonAddToCart_Click);
            // 
            // img
            // 
            this.img.Location = new System.Drawing.Point(27, 14);
            this.img.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.img.Name = "img";
            this.img.Size = new System.Drawing.Size(165, 165);
            this.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img.TabIndex = 33;
            this.img.TabStop = false;
            this.img.Click += new System.EventHandler(this.img_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(70, 196);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(0, 17);
            this.labelName.TabIndex = 43;
            // 
            // ProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelSoldOut);
            this.Controls.Add(this.buttonAddToCart);
            this.Controls.Add(this.img);
            this.Name = "ProductForm";
            this.Size = new System.Drawing.Size(223, 330);
            ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSoldOut;
        private System.Windows.Forms.Button buttonAddToCart;
        private System.Windows.Forms.PictureBox img;
        private System.Windows.Forms.Label labelName;
    }
}
