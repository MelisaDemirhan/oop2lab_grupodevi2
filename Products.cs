﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace OOPLAB2
{
    public class Products
    {
        private string name; //take name of product
        private int price; //take price of product
        private string description; //take description about product
        private string productImage; //take product's image's filename
        private int stock;
        private int id;
       
        public Products()
        {
            name = null;
            price = 0;
            description = null;
            productImage = null;
            stock = 0;
        }
        public Products(int id, string _name, int _price, string _description, string _productImage, int _stok)
        {
            this.id = id;
            name = _name;
            price = _price;
            description = _description;
            productImage = _productImage;
            stock = _stok;
        }
        public string Name { get { return name; } set { name = value; } }
        public int Price { get { return price; } set { price = value; } }
        public string Description { get { return description; } set { description = value; } }
        public string ProductImage { get { return productImage; } set { productImage = value; } }
        public int Stock
        {
            get { return stock; }
            set { stock = value; }
        }
        public int Id { get => id; set => id = value; }
    }
}
