﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLAB2
{
    public partial class ShoppingCrts : UserControl
    {
        Database database = new Database();
        List<Products> pp = new List<Products>();
        public ShoppingCrts()
        {
            InitializeComponent();
            labelPaymentAmount.Text = ShoppingCrt.cart.PaymentAmount.ToString() + " ₺";
            database.CartProductList();
            DataTable a = database.getCartProduct();
            database.ProductList();
            pp = database.getProductList();
            ShoppingCrt.cart = new Cart();
            for (int i = 0; i < a.Rows.Count; i++)
            {
                ItemToPurchase itemTP = new ItemToPurchase();
                foreach (var item in pp)
                {
                    if (Int32.Parse(a.Rows[i][1].ToString()) == item.Id)
                    {
                        itemTP.Product = item;
                        itemTP.Amount = Int32.Parse(a.Rows[i][2].ToString());
                        ShoppingCrt.cart.addProduct(itemTP);
                    }
                }
            }
            labelPaymentAmount.Text = ShoppingCrt.cart.PaymentAmount.ToString() + " ₺";
        }
    
        public static ShoppingCrts UploadShopListToCart()
        {
            ShoppingCrts items = new ShoppingCrts();
            foreach (ItemToPurchase item in Cart.ItemsToPurchase)
            {
                items.flowLayoutPanelCrts.Controls.Add(new ShoppingCrt(item));
            }
            items.Dock = DockStyle.Fill;
            return items;
        }   

        private void buttonDeleteCart_Click(object sender, EventArgs e)
        {
            ShoppingCrt.cart.cancelOrder();
            database.DeleteAllCartProductList();
            flowLayoutPanelCrts.Controls.Clear();
            labelPaymentAmount.Text = "0 ₺";
            MessageBox.Show("Your cart has been emptied");
        }

        private void buttonConfrimCart_Click(object sender, EventArgs e)
        {
            database.CartProductList();
            DataTable a = database.getCartProduct();
            database.ProductList();
            pp = database.getProductList();
            for (int i = 0; i < a.Rows.Count; i++)
            {              
                foreach (var item in pp)
                {
                    if (Int32.Parse(a.Rows[i][1].ToString()) == item.Id)
                    {
                        item.Stock = item.Stock - Int32.Parse(a.Rows[i][2].ToString());
                        database.UpdateProduct(item);
                    }
                }
            }
            ShoppingCrt.cart.cancelOrder();
            database.DeleteAllCartProductList();
            flowLayoutPanelCrts.Controls.Clear();
            labelPaymentAmount.Text = "0 ₺";
            MessageBox.Show("Purchase completed");
        }
    }
}
