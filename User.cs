﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLAB2
{
    public class User
    {
        private string userName;
        private string password;
        private Cart usercart;
        private string address;
        private string phoneNumber;
        private string profilePicture;
        private bool isAdmin;
        private int id;
     
        public User()
        {
            //usercart = new Cart();
            //isAdmin = false;
            //usercart = new Cart();
        }
        public User(string username,  string password, string address, int id, bool isAdmin, string pnumber,string pp)
        {
            userName = username;
            this.password = password;
            this.address = address;
            this.id = id;
            this.isAdmin = isAdmin;
            phoneNumber = pnumber;
            profilePicture = pp;
           // usercart = new Cart();
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public Cart Usercart
        {
            get { return usercart; }
            set { usercart = value; }
        }
        public string Address { get => address; set => address = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string ProfilePicture { get => profilePicture; set => profilePicture = value; }
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
        public int Id { get => id; set => id = value; }
    }
}
